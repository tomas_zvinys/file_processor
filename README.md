##Task description
###Develop an application capable of:
1. Parsing any number of .txt files at once. Files can contain any characters from the
   English alphabet, as well as numbers or special symbols
2. Counting a frequency of any word used among all input files
3. Saving the sorted result list (word and the number of times it’s been used) into four
   separate output files depending on the first letter of the word:
   1. Words beginning with the letter A-G
   2. Words beginning with the letter H-N
   3. Words beginning with the letter O-U
   4. Words beginning with the letter V-Z\
   Numeric or special symbols should be ignored during counting process
   
###Technical requirements:
- You are free to choose Java technologies for the task, but are required to use any
      popular development framework.
- Submitted source code should be organized and both easy to read and comprehend.
- Best practices for object-oriented or functional programming, code structure, reusability,
      expendability, simplicity or other core principles should be followed.
###Bonus objectives (optional):
1. Develop a web application with UI (React, Angular, Thymeleaf or other)
2. Upload any number of input files via UI
3. Display word count results in UI (Show four lists, provide download links or both)
4. Implement a software containerization solution, e.g. Docker
5. Host the application online

##Testing the application

Build & package application.\
Here's a way to do it with Maven (port 8080 will be used):
```shell
./mvnw package
```

... AND ...

Run application with maven ... 
```shell
./mvnw spring-boot:run
```

... OR ...

Run built application in docker (port 8070 will be used)
```shell
docker-compose up --build
```

