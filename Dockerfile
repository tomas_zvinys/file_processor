FROM amazoncorretto:11-alpine-jdk
ARG JAR_FILE=./target/file-processor.jar
COPY ${JAR_FILE} file-processor.jar
EXPOSE 80
ENTRYPOINT ["java","-jar","/file-processor.jar"]