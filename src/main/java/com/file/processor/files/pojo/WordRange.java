package com.file.processor.files.pojo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WordRange {
    private String boundMin;
    private String boundMax;

    public WordRange(String boundMin, String boundMax) {
        this.boundMin = boundMin;
        this.boundMax = boundMax;
    }
}
