package com.file.processor.files.pojo;

import lombok.Getter;
import lombok.experimental.Delegate;

import java.util.NavigableMap;

@Getter
public class WordCountSubGroup {
    private NavigableMap<String, Integer> wordCounts;
    @Delegate
    private final WordRange range;
    private final String fileName;

    public WordCountSubGroup(NavigableMap<String, Integer> nm, String fileName, WordRange wr){
        this.fileName = fileName;
        this.wordCounts = nm;
        this.range = wr;
    }

    public WordCountSubGroup(WordRange wr, String fileName) {
        this.fileName = fileName;
        this.range = wr;
    }

    public boolean isEmpty(){
        return this.wordCounts == null || this.wordCounts.isEmpty();
    }
}
