package com.file.processor.files.pojo;

import lombok.Getter;
import lombok.experimental.Delegate;

import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

@Getter
public class WordCountGroup {
    private final TreeMap<String, Integer> wordCounts;
    @Delegate
    private WordRange range;
    private String fileName;

    public WordCountGroup(WordRange range) {
        this.wordCounts = new TreeMap<>();
        this.range = range;
    }

    public WordCountGroup(TreeMap<String, Integer> in, String fileName){
        this.fileName = fileName;
        this.wordCounts = in;
    }

    public WordCountSubGroup getSubGroup(WordRange range){
        Map.Entry<String, Integer> minEntry = wordCounts.floorEntry(range.getBoundMin());
        Map.Entry<String, Integer> maxEntry = wordCounts.floorEntry(range.getBoundMax());

        if (maxEntry == null)
            return new WordCountSubGroup(range, fileName);

        if (minEntry == null)
            minEntry = wordCounts.firstEntry();
        String minKey = minEntry.getKey();
        String maxKey = maxEntry.getKey();

        boolean includeMinKey = minKey.compareTo(range.getBoundMin()) >= 0;

        NavigableMap<String, Integer> subGroup = wordCounts.subMap(minKey, includeMinKey, maxKey, true);
        return new WordCountSubGroup(subGroup, fileName, range);
    }

    public void add(WordCountSubGroup subGroup){
        if (!subGroup.isEmpty())
            for (Map.Entry<String, Integer> e : subGroup.getWordCounts().entrySet()) {
                String key = e.getKey();
                Integer value = e.getValue() + this.wordCounts.getOrDefault(key, 0);    //todo: can be replaced by merge()
                this.wordCounts.put(key, value);
            }
    }

    public int getTotalWordCount(){
        if (this.wordCounts == null || this.wordCounts.isEmpty())
            return 0;
        int total = 0;
        for (Map.Entry<String, Integer> e : wordCounts.entrySet())
            total += e.getValue();
        return total;
    }

}
