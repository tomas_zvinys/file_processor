package com.file.processor.files;


import com.file.processor.files.dto.BatchUploadDto;
import com.file.processor.files.pojo.WordCountGroup;
import com.file.processor.files.pojo.WordCountSubGroup;
import com.file.processor.files.pojo.WordRange;
import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FileService {
    private final List<WordRange> wordGroupBounds;
    private final BatchUploadRepository batchUploadRepository;
    private final WordStreamProcessor wordStreamProcessor;

    @Autowired
    public FileService(BatchUploadRepository batchUploadRepository, WordStreamProcessor wordStreamProcessor) {
        this.batchUploadRepository = batchUploadRepository;
        this.wordStreamProcessor = wordStreamProcessor;

        this.wordGroupBounds = new ArrayList<WordRange>() {{
            add(new WordRange("A", "G"));
            add(new WordRange("H", "N"));
            add(new WordRange("O", "U"));
            add(new WordRange("V", "Z"));
        }};
    }

    public NavigableMap<String, Integer> getBatchWordList(Integer batchId, String boundMin, String boundMax) {
        BatchUpload batchUpload = batchUploadRepository.getById(batchId);
        TreeMap<String, Integer> batchWordCounts = new TreeMap<>(batchUpload.getWordCounts());
        WordCountSubGroup wordGroup = new WordCountGroup(batchWordCounts, null)
                .getSubGroup(new WordRange(boundMin, boundMax));
        return wordGroup.getWordCounts();
    }

    public BatchUploadDto uploadWordFileBatch(HttpServletRequest request) throws FileUploadException {
        List<WordCountGroup> wordCountsPerFile = wordStreamProcessor.processFileUpload(request);
        List<WordCountGroup> wordCountGroups = groupWordCounts(wordGroupBounds, wordCountsPerFile);
        Integer totalWordCount = getTotalWords(wordCountGroups);

        TreeMap<String, Integer> allWordCounts = new TreeMap<>();
        for (WordCountGroup wordCountGroup : wordCountGroups)
            allWordCounts.putAll(wordCountGroup.getWordCounts());
        
        BatchUpload bu = new BatchUpload();
        bu.setWordCounts(allWordCounts);
        bu = batchUploadRepository.save(bu);

        BatchUploadDto bud = new BatchUploadDto();
        bud.setId(bu.getId());
        bud.setTotalWordCount(totalWordCount);
        bud.setWordCountGroups(wordCountGroups);
        return bud;
    }

    private int getTotalWords(List<WordCountGroup> wordGroups) {
        return wordGroups.stream().mapToInt(WordCountGroup::getTotalWordCount).sum();
    }

    private List<WordCountGroup> groupWordCounts(List<WordRange> wordGroups, List<WordCountGroup> wordCountsPerFile) {
        return wordGroups.parallelStream()
                .map(wordGroup -> {
                    WordCountGroup wcg = new WordCountGroup(wordGroup);
                    for (WordCountGroup fileWordCount : wordCountsPerFile)
                        wcg.add(fileWordCount.getSubGroup(wordGroup));
                    return wcg;
                })
                .collect(Collectors.toList());
    }

}
