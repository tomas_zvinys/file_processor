package com.file.processor.files.dto;

import com.file.processor.files.pojo.WordCountGroup;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BatchUploadDto {
    Integer id;
    Integer totalWordCount;
    List<WordCountGroup> wordCountGroups;
}
