package com.file.processor.files;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BatchUploadRepository extends JpaRepository<BatchUpload, Integer> {

}
