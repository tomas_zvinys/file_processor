package com.file.processor.files;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Map;
import java.util.TreeMap;

@Entity
@Table(name = "BATCH_UPLOAD")
@Getter
@Setter
public class BatchUpload {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @ElementCollection
    @CollectionTable(name = "BATCH_WORDS",
            joinColumns = {@JoinColumn(name = "BATCH_ID", referencedColumnName = "ID")})
    @MapKeyColumn(name = "WORD")
    @Column(name = "COUNT")
    private Map<String, Integer> wordCounts = new TreeMap<>();
}
