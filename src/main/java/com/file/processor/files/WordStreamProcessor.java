package com.file.processor.files;

import com.file.processor.files.pojo.WordCountGroup;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Component
public class WordStreamProcessor {
    private final ServletFileUpload upload;

    public WordStreamProcessor() {
        FileItemFactory factory = new DiskFileItemFactory();
        this.upload = new ServletFileUpload(factory);
    }


    public List<WordCountGroup> processFileUpload(HttpServletRequest request) throws FileUploadException {
        return getWordCountsPerFile(upload.parseRequest(request));
    }

    private List<WordCountGroup> getWordCountsPerFile(List<FileItem> fileItems) {
        return fileItems.parallelStream()
                .map(fileItem -> {
                    TreeMap<String, Integer> fileWordCounts = new TreeMap<>();
                    try {
                        fileWordCounts = this.countStreamWords(fileItem.getInputStream());
                    } catch (IOException ignore) {}
                    return new WordCountGroup(fileWordCounts, fileItem.getName());
                })
                .collect(Collectors.toList());
    }

    private TreeMap<String, Integer> countStreamWords(InputStream charStream) throws IOException {
        TreeMap<String, Integer> fileWordCounts = new TreeMap<>();
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(charStream));
        int c_int;

        do {
            c_int = reader.read();
            char c = (char) c_int;
            if (Character.isAlphabetic(c)) {
                sb.append(c);
            } else {
                if (sb.length() < 1)
                    continue;  //no word constructed - probably couple non-alphabetic chars
                String word = sb.toString().toUpperCase();
                Integer count = fileWordCounts.getOrDefault(word, 0);
                fileWordCounts.put(word, ++count);

                sb.setLength(0);
            }
        } while (c_int > -1);

        return fileWordCounts;
    }
}
