package com.file.processor.files;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.util.NavigableMap;


@Controller
@RequestMapping("/files")
public class FileController {

    private final FileService fileService;
    private final ObjectMapper mapper;

    public FileController(FileService fileService) {
        this.fileService = fileService;
        this.mapper = new ObjectMapper();
    }

    @GetMapping("/upload")
    public String displayUpload(HttpServletRequest request) {
        return "upload";
    }

    @PostMapping("/upload")
    public String handleUpload(RedirectAttributes model, HttpServletRequest request) throws FileUploadException {
        if (!ServletFileUpload.isMultipartContent(request))
            return "redirect:upload";

        model.addFlashAttribute("batchUpload", fileService.uploadWordFileBatch(request));

        return "redirect:upload";
    }

    @GetMapping("/download/{batchId}")
    public ResponseEntity<Resource> downloadBatchWords(@PathVariable Integer batchId,
                                                       @RequestParam String boundMin,
                                                       @RequestParam String boundMax) throws JsonProcessingException {
        NavigableMap<String, Integer> resource = fileService.getBatchWordList(batchId, boundMin, boundMax);
        String fileName = "WordCounts_" + batchId + "_[" + boundMin + "_" + boundMax + "].json";
        return ResponseEntity.ok()
                .header("Content-Disposition", "attachment; filename=" + fileName)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(new InputStreamResource(new ByteArrayInputStream(mapper.writeValueAsBytes(resource))));
    }
}
